import React from "react";
import Text from "./Text";
import Image from "./image";
import {useDispatch, useSelector} from "react-redux";
import {elementList, setCurrentId, updateMenu} from "../Redux/reducers/elements";

const Messenger = (props) => {
    const {id: messengerId} = props
    const elements = useSelector(elementList);
    const disPatch = useDispatch()

    const getComponent = (element) => {
        if (element.type === 'text') {
            return <Text showInput={false} val={element}/>
        } else if(element.type === 'image' && element.value !== 'https://i.stack.imgur.com/y9DpT.jpg') {
            return <Image showInput={false} val={element}/>
        }
    }

    const render = () => {
      const a=[];
      elements.map((element)=> {
          if(element.messengerId ===  messengerId && element.value) {
              a.push(<div key={element.id}>{getComponent(element)}</div>)
          }
      });
      return a
    }

    const onClickHandler = () => {
        disPatch(setCurrentId(messengerId));
        disPatch(updateMenu(true))
    }

    const openMenu = () => {
      disPatch(updateMenu(true))
    }

    return (
        <div className="messenger" onClick={onClickHandler}>
            <div className="header">Messenger</div>
            { elements.length ? render() : <div className="addText" onClick={openMenu}>
                Add element +
            </div> }
        </div>
    )}

export default Messenger