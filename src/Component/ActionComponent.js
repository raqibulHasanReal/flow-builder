import React from "react";
import {updateMenu} from "../Redux/reducers/elements";
import {useDispatch} from "react-redux";

const ActionComponent = () => {
    const disPatch = useDispatch();
    const onClickHandler = () => {
        disPatch(updateMenu(false))
    }

    return (
        <div className="action" onClick={onClickHandler}>
            <div className="actionHeader">
                Actions
            </div>
            <div className={"actionBody"}>
                Some Actions
            </div>
        </div>
    )
}

export default ActionComponent