import React, {useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {elementList, resetElement, setText, text,} from "../Redux/reducers/elements";

const Text = (props) => {
    const dispatch = useDispatch()
    const {showInput, val} = props;
    const value = useSelector(text);
    const elements = useSelector(elementList);
    const ref = useRef()

    const handleInput = (event) => {
        let {id,messengerId,type,value} = elements[elements.length-1]
        let newArray = [...elements]
        value = event.target.value;
        newArray.map((ele, index)=>{
            if(ele.id == id) {
                newArray.splice(index, 1);
            }
        })
        dispatch(setText(event.target.value));
        dispatch(resetElement([...newArray, {id,messengerId,type,value}]))
    }

    useEffect(() => {
        if (ref.current){
            ref.current.focus()
        }
    })

    return( <div>
        { showInput || <div className="text">{val.value}</div> }
        { showInput && <input ref={ref} className="textInput" type="text" value={value} onChange={handleInput}/> }
    </div>)
}

export default Text