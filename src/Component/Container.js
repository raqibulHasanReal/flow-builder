import React, {useRef, useState} from 'react';
import ReactFlow, {removeElements, addEdge, ReactFlowProvider, Controls} from 'react-flow-renderer';
import Sidebar from "./Sidebar";
import Messenger from "./Messenger";
import ActionComponent from "./ActionComponent";

let id = 1;
const getId = () => `${id++}`;

const Container = () => {
    const reactFlowWrapper = useRef(null);
    const [reactFlowInstance, setReactFlowInstance] = useState(null);
    const [elements, setElements] = useState([]);
    const onConnect = (params) => setElements((els) => addEdge(params, els));
    const onElementsRemove = (elementsToRemove) => setElements((els) => removeElements(elementsToRemove, els));
    const onLoad = (_reactFlowInstance) => setReactFlowInstance(_reactFlowInstance);
    const onDragOver = (event) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'move';
    };
    const onDrop = (event) => {
        event.preventDefault();

        const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
        const type = event.dataTransfer.getData('application/reactflow');
        const position = reactFlowInstance.project({
            x: event.clientX - reactFlowBounds.left,
            y: event.clientY - reactFlowBounds.top,
        });
        const id = getId();
        const newNode = {
            id: id,
            type: 'default',
            position,
            data: { label: type === 'Messenger' ? <Messenger id={id}/> : <ActionComponent/>},
        };

        setElements((es) => es.concat(newNode));
    };

    return (
        <div className="dndflow" style={{height: 900}}>
            <ReactFlowProvider>
                <div className="reactflow-wrapper" ref={reactFlowWrapper}>
                    <ReactFlow
                        elements={elements}
                        onConnect={onConnect}
                        onElementsRemove={onElementsRemove}
                        onLoad={onLoad}
                        onDrop={onDrop}
                        onDragOver={onDragOver}
                    >
                    <Controls />
                    </ReactFlow>
                </div>
                <Sidebar />
            </ReactFlowProvider>
        </div>
    );
}

export default Container;