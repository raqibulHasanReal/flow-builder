import React from 'react';
import {elementList, image, resetElement, setImage, setText} from "../Redux/reducers/elements";
import {useDispatch, useSelector} from "react-redux";

const Image = (props) => {
    const {showInput, val} = props;
    const img = useSelector(image);
    const dispatch = useDispatch();
    const elements = useSelector(elementList)

    const handleInput = (event) => {
        if (event.target.files && event.target.files[0]) {
            dispatch(setImage(URL.createObjectURL(event.target.files[0])));

            let {id,messengerId,type,value} = elements[elements.length-1]
            let newArray = [...elements]
            value = URL.createObjectURL(event.target.files[0]);
            newArray.map((ele, index)=>{
                if(ele.id == id) {
                    newArray.splice(index, 1);
                }
            })
            dispatch(resetElement([...newArray, {id,messengerId,type,value}]))
        }
    }
    console.log(val)
    return( <div>
        <div className="imageDiv"><img className="image" id="target" src={val ? val.value : img} alt='image'/></div>
        { showInput && <input type="file" accept="image/*" onChange={handleInput}/> }
    </div>)
}

export default Image