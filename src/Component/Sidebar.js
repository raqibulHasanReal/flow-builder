import React from 'react';
import Image from "./image";
import Text from "./Text";
import {useDispatch, useSelector} from "react-redux";
import {
    currentMessenger,
    imageSection,
    menuSection, setElements, setImage, setText,
    textMessageSection,
    updateImage,
    updateTextMessageSection
} from "../Redux/reducers/elements";

let id = 1;
const getId = () => `${id++}`;

export default () => {
    const menu = useSelector(menuSection);
    const image = useSelector(imageSection);
    const message = useSelector(textMessageSection);
    const currentMessengerId = useSelector(currentMessenger)
    const disPatch = useDispatch()
    const onDragStart = (event, nodeType) => {
        event.dataTransfer.setData('application/reactflow', nodeType);
        event.dataTransfer.effectAllowed = 'move';
    };

    const addImage = ()=> {
        disPatch(updateImage(true))
        disPatch(setImage('https://i.stack.imgur.com/y9DpT.jpg'));
        disPatch(setElements({ 'id': getId(), messengerId: currentMessengerId, 'type': 'image', value: 'https://i.stack.imgur.com/y9DpT.jpg' }))
    }

    const addMessage = ()=> {
        disPatch(setText(''));
        disPatch(updateTextMessageSection(true))
        disPatch(setElements({ 'id': getId(), messengerId: currentMessengerId, 'type': 'text', value: '' }))
    }

    return (
        <aside>
            <div className="description">You can drag these buttons to the pane on the left box.</div>
            <div className="dndnode input" onDragStart={(event) => onDragStart(event, 'Messenger')} draggable>
                Messenger
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'default')} draggable>
                Action
            </div>
            {menu && <div className="menuContainer">
                { image && <div className="innerMenu">
                    <Image showInput={true}/>
                </div>}
                { message && <div className="innerMenu">
                    <Text showInput={true}/>
                </div>}
                <div className="menu">
                    <div className="menuBox" onClick={addMessage}>Message</div>
                    <div className="menuBox" onClick={addImage}>Image</div>
                </div>
            </div>}
        </aside>
    );
};