import {createSlice} from '@reduxjs/toolkit';

const initialState = {
    image: 'https://i.stack.imgur.com/y9DpT.jpg',
    text: '',
    components: [],
    currentId: null,
    menu:  false,
    textMessageSection: false,
    imageSection: false
};

export const elements = createSlice({
    name: 'appointment',
    initialState,
    reducers: {
        setImage: (state, action) => {
            state.image = action.payload
        },
        setText:(state, action) => {
            state.text = action.payload
        },
        setElements:(state, action) => {
            state.components = [...state.components, action.payload]
        },
        resetElement:(state, action) => {
            state.components = action.payload
        },
        setCurrentId:(state, action) => {
            state.currentId = action.payload
        },
        updateMenu:(state, action) => {
            state.menu = action.payload
        },
        updateImage:(state, action) => {
            state.imageSection = action.payload;
            state.textMessageSection = false
        },
        updateTextMessageSection:(state, action) => {
            state.textMessageSection = action.payload
            state.imageSection = false
        }
    }
})
export const image = (state) => state.elements.image;
export const text = (state) => state.elements.text;
export const elementList = (state) => state.elements.components;
export const menuSection = (state) => state.elements.menu;
export const imageSection = (state) => state.elements.imageSection;
export const textMessageSection = (state) => state.elements.textMessageSection;
export const currentMessenger = (state) => state.elements.currentId;
export const { resetElement, setImage, setText, setCurrentId, setElements, updateMenu, updateImage, updateTextMessageSection } = elements.actions
export default elements.reducer