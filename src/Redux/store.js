import { configureStore } from '@reduxjs/toolkit';
import elements from "./reducers/elements";

export const store = configureStore({
    reducer: {
        elements: elements
    },
});